<?php
if(!isset($_SESSION['motyw'])){
    $_SESSION['motyw'];
}
?>
<script>
$(function() {
    var motyw = '<?php echo $_SESSION['motyw']; ?>';
    var zmienMotyw = function() {
        $.ajax({
            async: true,
            type: 'POST',
            cache: false,
            url: 'zaladuj_motyw.php',
            data: {
                dane: motyw
            },
            success: function(result) {
                motyw = result;
            }
        }).then(function() {
            motywStrony();
        });

    }
    var motywStrony = function() {
        if (motyw == 'ciemny') {
            $('body').css('background', 'brown');
        } else {
            $('body').css({
                background: "rgba(24,252,3,0.36)",
                background: "-moz-linear-gradient(-45deg, rgba(24,252,3,0.36) 0%, rgba(252,186,3,1) 100%)",
                background: "-webkit-gradient(left top, right bottom, color-stop(0%, rgba(24,252,3,0.36)), color-stop(100%, rgba(252,186,3,1)))",
                background: "-webkit-linear-gradient(-45deg, rgba(24,252,3,0.36) 0%, rgba(252,186,3,1) 100%)",
                background: "-o-linear-gradient(-45deg, rgba(24,252,3,0.36) 0%, rgba(252,186,3,1) 100%)",
                background: "-ms-linear-gradient(-45deg, rgba(24,252,3,0.36) 0%, rgba(252,186,3,1) 100%)",
                background: "linear-gradient(135deg, rgba(24,252,3,0.36) 0%, rgba(252,186,3,1) 100%)",
                filter: "progid:DXImageTransform.Microsoft.gradient( startColorstr='#18fc03', endColorstr='#fcba03', GradientType=1 )"
            });
            $('body').css('background-attachment', 'fixed');
        }
    }
    motywStrony();
    if (motyw == 'jasny') {

    } else {
        $('.motywSwitch').css({
            'left': '22px'
        });
    }
    $('.motywRamka').click(function() {
        zmienMotyw();
        var x = $('.motywSwitch').position();
        if (x.left != 0) {
            $('.motywSwitch').animate({
                'left': '0px'
            });
        } else {
            $('.motywSwitch').animate({
                'left': '22px'
            });
        }
    });
});
</script>
<?php
if(isset($_SERVER['HTTP_REFERER']) && !empty($_POST)){
    echo '<a class="backButton" href='.$_SERVER['HTTP_REFERER'].'>Powrót</a>';
}

echo '<div id="motyw">';
    echo '<p>J</p>';
    echo '<div class="motywRamka">';
        echo '<div class="motywSwitch"></div>';
    echo '</div>';
    echo '<p>C</p>';
echo '</div>';