<?php

$host = 'localhost';
$user = 'root';
$pass = '';
$db = 'knittapp';

$connect = mysqli_connect($host, $user, $pass, $db);
mysqli_query($connect, "SET CHARACTER SET utf8");


if (!$connect) {
    echo 'Błąd podczas łączenia z bazą.<br>';
    echo 'Numer błędu: '.mysqli_connect_errno().'<br>';
    echo 'Komunikat błęu: '.mysqli_connect_error().'<br>';
    exit;
}else {
    //echo 'Połączono się z bazą danych. Host: '.mysqli_get_host_info($connect).'<br><br>';
}

