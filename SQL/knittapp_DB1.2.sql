-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 23 Sie 2019, 22:06
-- Wersja serwera: 10.1.36-MariaDB
-- Wersja PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `knittapp`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `strony_baza`
--

CREATE TABLE `strony_baza` (
  `id_strona` int(11) NOT NULL,
  `nazwa` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `typ_strony` int(11) NOT NULL COMMENT '1-kategorie,2-strony,3-mapa',
  `kategoria` text NOT NULL,
  `podkategorieBOX` varchar(255) NOT NULL,
  `podkategorieZNACZNIK` varchar(10) NOT NULL,
  `podkategorieNAZWA` varchar(255) NOT NULL,
  `itemBOX` varchar(255) NOT NULL,
  `itemZNACZNIK` varchar(10) NOT NULL,
  `itemNAZWA` varchar(255) NOT NULL,
  `paginacjaBOX` varchar(255) NOT NULL,
  `paginacjaZNACZNIK` varchar(10) NOT NULL,
  `paginacjaNAZWA` varchar(255) NOT NULL,
  `paginacjaELEMENT` varchar(25) NOT NULL,
  `paginacjaGET` varchar(255) NOT NULL,
  `tytulBOX` varchar(255) NOT NULL,
  `tytulZNACZNIK` varchar(10) NOT NULL,
  `tytulNAZWA` varchar(255) NOT NULL,
  `tytulELEMENT` varchar(25) NOT NULL,
  `cenaBOX` varchar(255) NOT NULL,
  `cenaZNACZNIK` varchar(10) NOT NULL,
  `cenaNAZWA` varchar(255) NOT NULL,
  `cenaELEMENT` varchar(25) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `strony_baza`
--

INSERT INTO `strony_baza` (`id_strona`, `nazwa`, `link`, `active`, `typ_strony`, `kategoria`, `podkategorieBOX`, `podkategorieZNACZNIK`, `podkategorieNAZWA`, `itemBOX`, `itemZNACZNIK`, `itemNAZWA`, `paginacjaBOX`, `paginacjaZNACZNIK`, `paginacjaNAZWA`, `paginacjaELEMENT`, `paginacjaGET`, `tytulBOX`, `tytulZNACZNIK`, `tytulNAZWA`, `tytulELEMENT`, `cenaBOX`, `cenaZNACZNIK`, `cenaNAZWA`, `cenaELEMENT`, `image`) VALUES
(4, 'fastryga.pl', 'https://www.fastryga.pl/', 0, 1, 'category/wloczki', 'div', '#', 'subcategories', 'div', '.', 'item', 'div', '.', 'pages', '', '/', 'div', '.', 'name', 'a', 'div', '.', 'price', 'span', 'img'),
(5, 'sklep-ik.pl', 'https://www.sklep-ik.pl/', 1, 2, 'kategoria-produktu/wloczki/', 'div', '.', 'ss - cokolwiek zeby nie szukalo podkategori tylko lecialo stron', 'div', '.', 'product-item-wrapper', 'ul', '.', 'pagination', 'a', 'page/', 'a', '.', 'product-name', '', 'span', '.', 'price', 'span', 'img'),
(7, 'e-dziewiarka.pl/', 'https://www.e-dziewiarka.pl/', 0, 1, 'sklep', 'div', '.', 'category-view', 'div', '.', 'produkt', 'span', '.', 'boxprice', '', '', 'div', '.', 'spacer', 'a', 'div', '.', 'PricesalesPrice', 'span[2]', 'img');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `strony_log`
--

CREATE TABLE `strony_log` (
  `id_log` int(11) NOT NULL,
  `id_strony` int(11) NOT NULL,
  `data_aktualizacji` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `strony_log`
--

INSERT INTO `strony_log` (`id_log`, `id_strony`, `data_aktualizacji`) VALUES
(1, 5, '2019-08-23 21:09:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wloczki_szczeg`
--

CREATE TABLE `wloczki_szczeg` (
  `id_wloczki` int(11) NOT NULL,
  `nazwa_strony` varchar(255) NOT NULL,
  `nazwa_wloczki` text NOT NULL,
  `cena` decimal(9,2) NOT NULL,
  `obraz` text NOT NULL,
  `data_dodania` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `wloczki_szczeg`
--

INSERT INTO `wloczki_szczeg` (`id_wloczki`, `nazwa_strony`, `nazwa_wloczki`, `cena`, `obraz`, `data_dodania`) VALUES
(73, 'sklep-ik.pl', 'DROPS Air (01) Ecru', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_01-268x415.jpg', '2019-08-23 21:09:00'),
(74, 'sklep-ik.pl', 'DROPS Air (02) mix Pszeniczny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_02_mix-268x415.jpg', '2019-08-23 21:09:00'),
(75, 'sklep-ik.pl', 'DROPS Air (03) mix Perłowy szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_03-268x415.jpg', '2019-08-23 21:09:00'),
(76, 'sklep-ik.pl', 'DROPS Air (04) mix Średni szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_04_mix-268x415.jpg', '2019-08-23 21:09:00'),
(77, 'sklep-ik.pl', 'DROPS Air (05) mix Brąz', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_05_mix-268x415.jpg', '2019-08-23 21:09:00'),
(78, 'sklep-ik.pl', 'DROPS Air (06) mix Czarny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_06_mix-268x415.jpg', '2019-08-23 21:09:00'),
(79, 'sklep-ik.pl', 'DROPS Air (07) mix Rubinowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_07_mix-268x415.jpg', '2019-08-23 21:09:00'),
(80, 'sklep-ik.pl', 'DROPS Air (08) mix Jasny róż', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_08_mix-268x415.jpg', '2019-08-23 21:09:00'),
(81, 'sklep-ik.pl', 'DROPS Air (09) mix Niebieski morski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_09_mix-268x415.jpg', '2019-08-23 21:09:00'),
(82, 'sklep-ik.pl', 'DROPS Air (10) mix Mgła', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_10_mix-268x415.jpg', '2019-08-23 21:09:00'),
(83, 'sklep-ik.pl', 'DROPS Air (11) mix Zielononiebieski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_11_mix-268x415.jpg', '2019-08-23 21:09:00'),
(84, 'sklep-ik.pl', 'DROPS Air (13) mix Pomarańczowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_13_mix-268x415.jpg', '2019-08-23 21:09:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wloczki_szczeg_log`
--

CREATE TABLE `wloczki_szczeg_log` (
  `id_log` int(11) NOT NULL,
  `nazwa_strony` varchar(255) NOT NULL,
  `nazwa_wloczki` text NOT NULL,
  `cena` decimal(9,2) NOT NULL,
  `obraz` text NOT NULL,
  `data_dodania` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `wloczki_szczeg_log`
--

INSERT INTO `wloczki_szczeg_log` (`id_log`, `nazwa_strony`, `nazwa_wloczki`, `cena`, `obraz`, `data_dodania`) VALUES
(1, 'sklep-ik.pl', 'DROPS Air (01) Ecru', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_01-268x415.jpg', '0000-00-00 00:00:00'),
(2, 'sklep-ik.pl', 'DROPS Air (02) mix Pszeniczny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_02_mix-268x415.jpg', '0000-00-00 00:00:00'),
(3, 'sklep-ik.pl', 'DROPS Air (03) mix Perłowy szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_03-268x415.jpg', '0000-00-00 00:00:00'),
(4, 'sklep-ik.pl', 'DROPS Air (04) mix Średni szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_04_mix-268x415.jpg', '0000-00-00 00:00:00'),
(5, 'sklep-ik.pl', 'DROPS Air (05) mix Brąz', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_05_mix-268x415.jpg', '0000-00-00 00:00:00'),
(6, 'sklep-ik.pl', 'DROPS Air (06) mix Czarny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_06_mix-268x415.jpg', '0000-00-00 00:00:00'),
(7, 'sklep-ik.pl', 'DROPS Air (07) mix Rubinowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_07_mix-268x415.jpg', '0000-00-00 00:00:00'),
(8, 'sklep-ik.pl', 'DROPS Air (08) mix Jasny róż', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_08_mix-268x415.jpg', '0000-00-00 00:00:00'),
(9, 'sklep-ik.pl', 'DROPS Air (09) mix Niebieski morski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_09_mix-268x415.jpg', '0000-00-00 00:00:00'),
(10, 'sklep-ik.pl', 'DROPS Air (10) mix Mgła', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_10_mix-268x415.jpg', '0000-00-00 00:00:00'),
(11, 'sklep-ik.pl', 'DROPS Air (11) mix Zielononiebieski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_11_mix-268x415.jpg', '0000-00-00 00:00:00'),
(12, 'sklep-ik.pl', 'DROPS Air (13) mix Pomarańczowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_13_mix-268x415.jpg', '0000-00-00 00:00:00'),
(13, 'sklep-ik.pl', 'DROPS Air (01) Ecru', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_01-268x415.jpg', '0000-00-00 00:00:00'),
(14, 'sklep-ik.pl', 'DROPS Air (02) mix Pszeniczny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_02_mix-268x415.jpg', '0000-00-00 00:00:00'),
(15, 'sklep-ik.pl', 'DROPS Air (03) mix Perłowy szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_03-268x415.jpg', '0000-00-00 00:00:00'),
(16, 'sklep-ik.pl', 'DROPS Air (04) mix Średni szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_04_mix-268x415.jpg', '0000-00-00 00:00:00'),
(17, 'sklep-ik.pl', 'DROPS Air (05) mix Brąz', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_05_mix-268x415.jpg', '0000-00-00 00:00:00'),
(18, 'sklep-ik.pl', 'DROPS Air (06) mix Czarny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_06_mix-268x415.jpg', '0000-00-00 00:00:00'),
(19, 'sklep-ik.pl', 'DROPS Air (07) mix Rubinowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_07_mix-268x415.jpg', '0000-00-00 00:00:00'),
(20, 'sklep-ik.pl', 'DROPS Air (08) mix Jasny róż', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_08_mix-268x415.jpg', '0000-00-00 00:00:00'),
(21, 'sklep-ik.pl', 'DROPS Air (09) mix Niebieski morski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_09_mix-268x415.jpg', '0000-00-00 00:00:00'),
(22, 'sklep-ik.pl', 'DROPS Air (10) mix Mgła', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_10_mix-268x415.jpg', '0000-00-00 00:00:00'),
(23, 'sklep-ik.pl', 'DROPS Air (11) mix Zielononiebieski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_11_mix-268x415.jpg', '0000-00-00 00:00:00'),
(24, 'sklep-ik.pl', 'DROPS Air (13) mix Pomarańczowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_13_mix-268x415.jpg', '0000-00-00 00:00:00'),
(25, 'sklep-ik.pl', 'DROPS Air (01) Ecru', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_01-268x415.jpg', '0000-00-00 00:00:00'),
(26, 'sklep-ik.pl', 'DROPS Air (02) mix Pszeniczny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_02_mix-268x415.jpg', '0000-00-00 00:00:00'),
(27, 'sklep-ik.pl', 'DROPS Air (03) mix Perłowy szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_03-268x415.jpg', '0000-00-00 00:00:00'),
(28, 'sklep-ik.pl', 'DROPS Air (04) mix Średni szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_04_mix-268x415.jpg', '0000-00-00 00:00:00'),
(29, 'sklep-ik.pl', 'DROPS Air (05) mix Brąz', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_05_mix-268x415.jpg', '0000-00-00 00:00:00'),
(30, 'sklep-ik.pl', 'DROPS Air (06) mix Czarny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_06_mix-268x415.jpg', '0000-00-00 00:00:00'),
(31, 'sklep-ik.pl', 'DROPS Air (07) mix Rubinowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_07_mix-268x415.jpg', '0000-00-00 00:00:00'),
(32, 'sklep-ik.pl', 'DROPS Air (08) mix Jasny róż', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_08_mix-268x415.jpg', '0000-00-00 00:00:00'),
(33, 'sklep-ik.pl', 'DROPS Air (09) mix Niebieski morski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_09_mix-268x415.jpg', '0000-00-00 00:00:00'),
(34, 'sklep-ik.pl', 'DROPS Air (10) mix Mgła', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_10_mix-268x415.jpg', '0000-00-00 00:00:00'),
(35, 'sklep-ik.pl', 'DROPS Air (11) mix Zielononiebieski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_11_mix-268x415.jpg', '0000-00-00 00:00:00'),
(36, 'sklep-ik.pl', 'DROPS Air (13) mix Pomarańczowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_13_mix-268x415.jpg', '0000-00-00 00:00:00'),
(37, 'sklep-ik.pl', 'DROPS Air (01) Ecru', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_01-268x415.jpg', '2019-08-23 21:04:14'),
(38, 'sklep-ik.pl', 'DROPS Air (02) mix Pszeniczny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_02_mix-268x415.jpg', '2019-08-23 21:04:14'),
(39, 'sklep-ik.pl', 'DROPS Air (03) mix Perłowy szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_03-268x415.jpg', '2019-08-23 21:04:14'),
(40, 'sklep-ik.pl', 'DROPS Air (04) mix Średni szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_04_mix-268x415.jpg', '2019-08-23 21:04:14'),
(41, 'sklep-ik.pl', 'DROPS Air (05) mix Brąz', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_05_mix-268x415.jpg', '2019-08-23 21:04:14'),
(42, 'sklep-ik.pl', 'DROPS Air (06) mix Czarny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_06_mix-268x415.jpg', '2019-08-23 21:04:14'),
(43, 'sklep-ik.pl', 'DROPS Air (07) mix Rubinowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_07_mix-268x415.jpg', '2019-08-23 21:04:14'),
(44, 'sklep-ik.pl', 'DROPS Air (08) mix Jasny róż', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_08_mix-268x415.jpg', '2019-08-23 21:04:14'),
(45, 'sklep-ik.pl', 'DROPS Air (09) mix Niebieski morski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_09_mix-268x415.jpg', '2019-08-23 21:04:14'),
(46, 'sklep-ik.pl', 'DROPS Air (10) mix Mgła', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_10_mix-268x415.jpg', '2019-08-23 21:04:14'),
(47, 'sklep-ik.pl', 'DROPS Air (11) mix Zielononiebieski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_11_mix-268x415.jpg', '2019-08-23 21:04:14'),
(48, 'sklep-ik.pl', 'DROPS Air (13) mix Pomarańczowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_13_mix-268x415.jpg', '2019-08-23 21:04:14'),
(49, 'sklep-ik.pl', 'DROPS Air (01) Ecru', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_01-268x415.jpg', '2019-08-23 21:07:30'),
(50, 'sklep-ik.pl', 'DROPS Air (02) mix Pszeniczny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_02_mix-268x415.jpg', '2019-08-23 21:07:30'),
(51, 'sklep-ik.pl', 'DROPS Air (03) mix Perłowy szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_03-268x415.jpg', '2019-08-23 21:07:30'),
(52, 'sklep-ik.pl', 'DROPS Air (04) mix Średni szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_04_mix-268x415.jpg', '2019-08-23 21:07:30'),
(53, 'sklep-ik.pl', 'DROPS Air (05) mix Brąz', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_05_mix-268x415.jpg', '2019-08-23 21:07:30'),
(54, 'sklep-ik.pl', 'DROPS Air (06) mix Czarny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_06_mix-268x415.jpg', '2019-08-23 21:07:30'),
(55, 'sklep-ik.pl', 'DROPS Air (07) mix Rubinowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_07_mix-268x415.jpg', '2019-08-23 21:07:30'),
(56, 'sklep-ik.pl', 'DROPS Air (08) mix Jasny róż', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_08_mix-268x415.jpg', '2019-08-23 21:07:30'),
(57, 'sklep-ik.pl', 'DROPS Air (09) mix Niebieski morski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_09_mix-268x415.jpg', '2019-08-23 21:07:30'),
(58, 'sklep-ik.pl', 'DROPS Air (10) mix Mgła', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_10_mix-268x415.jpg', '2019-08-23 21:07:30'),
(59, 'sklep-ik.pl', 'DROPS Air (11) mix Zielononiebieski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_11_mix-268x415.jpg', '2019-08-23 21:07:30'),
(60, 'sklep-ik.pl', 'DROPS Air (13) mix Pomarańczowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_13_mix-268x415.jpg', '2019-08-23 21:07:30'),
(61, 'sklep-ik.pl', 'DROPS Air (01) Ecru', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_01-268x415.jpg', '2019-08-23 21:09:00'),
(62, 'sklep-ik.pl', 'DROPS Air (02) mix Pszeniczny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_02_mix-268x415.jpg', '2019-08-23 21:09:00'),
(63, 'sklep-ik.pl', 'DROPS Air (03) mix Perłowy szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_03-268x415.jpg', '2019-08-23 21:09:00'),
(64, 'sklep-ik.pl', 'DROPS Air (04) mix Średni szary', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_04_mix-268x415.jpg', '2019-08-23 21:09:00'),
(65, 'sklep-ik.pl', 'DROPS Air (05) mix Brąz', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_05_mix-268x415.jpg', '2019-08-23 21:09:00'),
(66, 'sklep-ik.pl', 'DROPS Air (06) mix Czarny', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_06_mix-268x415.jpg', '2019-08-23 21:09:00'),
(67, 'sklep-ik.pl', 'DROPS Air (07) mix Rubinowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_07_mix-268x415.jpg', '2019-08-23 21:09:00'),
(68, 'sklep-ik.pl', 'DROPS Air (08) mix Jasny róż', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_08_mix-268x415.jpg', '2019-08-23 21:09:00'),
(69, 'sklep-ik.pl', 'DROPS Air (09) mix Niebieski morski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_09_mix-268x415.jpg', '2019-08-23 21:09:00'),
(70, 'sklep-ik.pl', 'DROPS Air (10) mix Mgła', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_10_mix-268x415.jpg', '2019-08-23 21:09:00'),
(71, 'sklep-ik.pl', 'DROPS Air (11) mix Zielononiebieski', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_11_mix-268x415.jpg', '2019-08-23 21:09:00'),
(72, 'sklep-ik.pl', 'DROPS Air (13) mix Pomarańczowy', '22.00', 'https://www.sklep-ik.pl/wp-content/uploads/2018/10/DROPS_AIR_13_mix-268x415.jpg', '2019-08-23 21:09:00');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `strony_baza`
--
ALTER TABLE `strony_baza`
  ADD PRIMARY KEY (`id_strona`);

--
-- Indeksy dla tabeli `strony_log`
--
ALTER TABLE `strony_log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indeksy dla tabeli `wloczki_szczeg`
--
ALTER TABLE `wloczki_szczeg`
  ADD PRIMARY KEY (`id_wloczki`);

--
-- Indeksy dla tabeli `wloczki_szczeg_log`
--
ALTER TABLE `wloczki_szczeg_log`
  ADD PRIMARY KEY (`id_log`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `strony_baza`
--
ALTER TABLE `strony_baza`
  MODIFY `id_strona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `strony_log`
--
ALTER TABLE `strony_log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `wloczki_szczeg`
--
ALTER TABLE `wloczki_szczeg`
  MODIFY `id_wloczki` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT dla tabeli `wloczki_szczeg_log`
--
ALTER TABLE `wloczki_szczeg_log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
