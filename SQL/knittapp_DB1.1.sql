-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 13 Sie 2019, 22:11
-- Wersja serwera: 10.1.36-MariaDB
-- Wersja PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `knittapp`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `strony_baza`
--

CREATE TABLE `strony_baza` (
  `id_strona` int(11) NOT NULL,
  `nazwa` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `kategoria` text NOT NULL,
  `podkategorieBOX` varchar(255) NOT NULL,
  `podkategorieZNACZNIK` varchar(10) NOT NULL,
  `podkategorieNAZWA` varchar(255) NOT NULL,
  `itemBOX` varchar(255) NOT NULL,
  `itemZNACZNIK` varchar(10) NOT NULL,
  `itemNAZWA` varchar(255) NOT NULL,
  `paginacjaBOX` varchar(255) NOT NULL,
  `paginacjaZNACZNIK` varchar(10) NOT NULL,
  `paginacjaNAZWA` varchar(255) NOT NULL,
  `paginacjaELEMENT` varchar(25) NOT NULL,
  `paginacjaGET` varchar(255) NOT NULL,
  `tytulBOX` varchar(255) NOT NULL,
  `tytulZNACZNIK` varchar(10) NOT NULL,
  `tytulNAZWA` varchar(255) NOT NULL,
  `tytulELEMENT` varchar(25) NOT NULL,
  `cenaBOX` varchar(255) NOT NULL,
  `cenaZNACZNIK` varchar(10) NOT NULL,
  `cenaNAZWA` varchar(255) NOT NULL,
  `cenaELEMENT` varchar(25) NOT NULL,
  `image` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `strony_baza`
--

INSERT INTO `strony_baza` (`id_strona`, `nazwa`, `link`, `kategoria`, `podkategorieBOX`, `podkategorieZNACZNIK`, `podkategorieNAZWA`, `itemBOX`, `itemZNACZNIK`, `itemNAZWA`, `paginacjaBOX`, `paginacjaZNACZNIK`, `paginacjaNAZWA`, `paginacjaELEMENT`, `paginacjaGET`, `tytulBOX`, `tytulZNACZNIK`, `tytulNAZWA`, `tytulELEMENT`, `cenaBOX`, `cenaZNACZNIK`, `cenaNAZWA`, `cenaELEMENT`, `image`, `active`) VALUES
(1, 'fastryga.pl', 'https://www.fastryga.pl/', 'category/wloczki', 'subcategories', '', '', 'item', '', '', 'pages', '', '', '', '', 'name', '', '', '', 'price', '', '', '', '', 0),
(2, 'fastryga.pl', 'https://www.fastryga.pl/', 'category/akcesoria-do-robotek-1452', 'subcategories', '', '', 'item', '', '', 'pages', '', '', '', '', 'name', '', '', '', 'price', '', '', '', '', 0),
(3, 'sklep-ik.pl', 'https://www.sklep-ik.pl/', 'kategoria-produktu/wloczki/', 'product-listing', '', '', 'product-item-wrapper', '', '', 'page-numbers', '', '', '', '', 'product-thumb', '', '', '', 'price', '', '', '', '', 0),
(4, 'fastryga.pl', 'https://www.fastryga.pl/', 'category/wloczki', 'div', '#', 'subcategories', 'div', '.', 'item', 'div', '.', 'pages', '', '', 'div', '.', 'name', 'a', 'div', '.', 'price', 'span', 'img', 1),
(5, 'sklep-ik.pl', 'https://www.sklep-ik.pl/', 'kategoria-produktu/wloczki/', 'div', '.', 'ss - cokolwiek zeby nie szukalo podkategori tylko lecialo stron', 'div', '.', 'product-item-wrapper', 'ul', '.', 'pagination', 'a', 'page', 'a', '.', 'product-name', '', 'span', '.', 'price', 'span', 'img', 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `strony_baza`
--
ALTER TABLE `strony_baza`
  ADD PRIMARY KEY (`id_strona`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `strony_baza`
--
ALTER TABLE `strony_baza`
  MODIFY `id_strona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
