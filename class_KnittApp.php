<?php

class knittApp {
	
    public $kategorie;
    public $ileDanych;

	function __construct(/*$strona = null*/){
        //include 'polaczenie_baza.php';
		// if ( $strona ) {
		// 	$this->dane_baza($strona);
        // }
        //$this->dane = array();
        //$this->licznik = 0;
        //$this->kategorie = array();
	}
    
    
	public function dane_baza($strona = ''){
        include 'polaczenie_baza.php';
        if($strona == ''){ $zpt = 'SELECT * FROM strony_baza WHERE active = 1'; }
        else{ $zpt = 'SELECT * FROM strony_baza WHERE active = 1 AND nazwa = "'.$strona.'"'; }
        $rezultat = mysqli_query($connect, $zpt);

        if(mysqli_num_rows($rezultat) > 0){
            $indeks = 0;
            while($wynik = mysqli_fetch_assoc($rezultat)){
                $this->firmy[$indeks] = $wynik;
                $indeks++;
            }
            mysqli_free_result($rezultat);
            return $this->firmy;
        }else{
            return;
        }
    }

    private function zapisz_do_bazy($dane, $nazwa){
        include 'polaczenie_baza.php';
        $zptStrony = "SELECT * FROM strony_baza WHERE nazwa='".$nazwa."'";
        $resultStrony = mysqli_query($connect, $zptStrony);
        while($wynik = mysqli_fetch_assoc($resultStrony)){
            $idStronyDoLoga = $wynik['id_strona'];
        }
        $zptLog = "INSERT INTO strony_log (`id_strony`, `data_aktualizacji`) VALUES (".$idStronyDoLoga.", NOW())";
        $resultLog = mysqli_query($connect, $zptLog);

        $zpt = "INSERT INTO wloczki_szczeg_log (`nazwa_strony`, `nazwa_wloczki`, `cena`, `obraz`, `data_dodania`) VALUES ";
        foreach($dane as $wartosci){
            $zpt .= "('".$nazwa."', '".$wartosci['nazwa']."', '".$wartosci['cena']."', '".$wartosci['img']."', NOW()), ";
        }
        $zpt = substr($zpt, 0, strlen($zpt)-2);
        $result = mysqli_query($connect, $zpt);

        $zpt = "SELECT * FROM wloczki_szczeg WHERE nazwa_strony='".$nazwa."'";
        $result = mysqli_query($connect, $zpt);
        if($result && mysqli_num_rows($result) > 0){
            $this->usun_z_bazy($dane, $nazwa);
        }

        $zpt1 = "INSERT INTO wloczki_szczeg (`nazwa_strony`, `nazwa_wloczki`, `cena`, `obraz`, `data_dodania`) VALUES ";
        foreach($dane as $wartosci){
            $zpt1 .= "('".$nazwa."', '".$wartosci['nazwa']."', '".$wartosci['cena']."', '".$wartosci['img']."', NOW()), ";
        } 
        $zpt1 = substr($zpt1, 0, strlen($zpt1)-2);
        $result1 = mysqli_query($connect, $zpt1);
    }

    private function usun_z_bazy($dane, $nazwa){
        include 'polaczenie_baza.php';
        
        $zpt1 = "DELETE FROM wloczki_szczeg WHERE nazwa_strony='".$nazwa."'";
        $result1 = mysqli_query($connect, $zpt1);
    }

    private function pobierz_strony($link, $kategoria, $box, $znacznik, $nazwa, $element){
        $html = file_get_html($link.$kategoria);
        $kolumny = [];
        // echo $box.$znacznik.$nazwa.' '.$element;
        if($html->find($box.$znacznik.$nazwa)){
            foreach($html->find($box.$znacznik.$nazwa.' '.$element) as $row){
                $stronaB = preg_replace("/[^0-9,.]/", " ", $row->plaintext);
                $ex = explode(' ', $stronaB);
                array_push($kolumny, $row->plaintext);
                $kolumny = array_merge($ex, $kolumny);
            }
            $stron = max($kolumny);
        }elseif(true){
            $stron = 0;
        }
        return $stron;
    }

    private function pobierz_czas_strony($strona){
        $strony = $this->pobierz_strony($strona['link'], $strona['kategoria'], $strona['paginacjaBOX'], $strona['paginacjaZNACZNIK'], $strona['paginacjaNAZWA'], $strona['paginacjaELEMENT']);
        echo 'Ilość stron do załadowania: '.$strony.'<br>';
    }

    private function pobierz_czas_kategorie($strona){
        $html = file_get_html($strona['link'].$strona['kategoria']);
        $ilePodkategorii = 0;
        if($html->find($strona['podkategorieBOX'].$strona['podkategorieZNACZNIK'].$strona['podkategorieNAZWA'])){
            foreach ($html->find($strona['podkategorieBOX'].$strona['podkategorieZNACZNIK'].$strona['podkategorieNAZWA'].' '.$strona['tytulBOX'].$strona['tytulZNACZNIK'].$strona['tytulNAZWA'].' '.$strona['tytulELEMENT']) as $row) {
                $ilePodkategorii++;
            }
        }
        echo 'Ilość podstron do załadowania: '.$ilePodkategorii.'<br>';
    }
    
    public function pobierz_czas($tablicaDanych = ''){
        if($tablicaDanych == ''){
            $daneBaza = $this->dane_baza();
            $ileBaza = count($daneBaza);
            for($i = 0; $i < $ileBaza; $i++){
                if($daneBaza[$i]['typ_strony'] == 1){
                    $dane = $this->pobierz_czas_kategorie($daneBaza[$i]);
                }elseif($daneBaza[$i]['typ_strony'] == 2){
                    $dane = $this->pobierz_czas_strony($daneBaza[$i]);
                }elseif($daneBaza[$i]['typ_strony'] == 3){
                    $dane = $this->pobierz_czas_mapa($daneBaza[$i]);
                }
            }
        }else{
            $dane = '';
            $daneBaza = $this->dane_baza($tablicaDanych);
            if($daneBaza[0]['typ_strony'] == 1){
                $dane = $this->pobierz_czas_kategorie($daneBaza[0]);
            }elseif($daneBaza[0]['typ_strony'] == 2){
                $dane = $this->pobierz_czas_strony($daneBaza[0]);
            }elseif($daneBaza[0]['typ_strony'] == 3){
                $dane = $this->pobierz_czas_mapa($daneBaza[0]);
            }
        }
    }

    public function pokaz_wyniki($dane, $link){
        $str = '';
        foreach((array)$dane as $el){
            $str .= '<div class="item">';
            foreach($el as $id => $wartosc){ 
                if($id == 'img'){
                    if(strpos($wartosc, 'http') !==false ){ // jesli w linku zjdecia znajduje sie baza strony to omin ja
                        $str .= '<img width=50 height=50 src="'.$wartosc.'">';
                    }else{
                        $str .= '<img width=50 height=50 src="'.$link.$wartosc.'">';
                    }
                }else {
                    $str .= $id.': '.$wartosc.' ';
                }
            }
            $str .= '</div>';
        }
        // json_encode($str);
        echo $str;
    }

    public function laduj_wszystko(){
        $daneBaza = $this->dane_baza();
        $ileBaza = count($daneBaza);
        for($i = 0; $i < $ileBaza; $i++){
            if($daneBaza[$i]['typ_strony'] == 1){
                $dane = $this->pobierz_strony_kategorie($daneBaza[$i]);
            }elseif($daneBaza[$i]['typ_strony'] == 2){
                $dane = $this->pobierz_strony_strony($daneBaza[$i]);
            }elseif($daneBaza[$i]['typ_strony'] == 3){
                $dane = $this->pobierz_strony_mapa($daneBaza[$i]);
            }
            if($dane != ''){
                $this->zapisz_do_bazy($dane, $daneBaza[$i]['nazwa']);
                $this->pokaz_wyniki($dane, $daneBaza[$i]['link']);
            }else{
                echo 'Brak danych do zapisania.';
            }
        }
    }

    public function laduj_wybrane($tablicaDanych = ''){
        $dane = '';
        $daneBaza = $this->dane_baza($tablicaDanych);
        if($daneBaza[0]['typ_strony'] == 1){
            $dane = $this->pobierz_strony_kategorie($daneBaza[0]);
        }elseif($daneBaza[0]['typ_strony'] == 2){
            $dane = $this->pobierz_strony_strony($daneBaza[0]);
        }elseif($daneBaza[0]['typ_strony'] == 3){
            $dane = $this->pobierz_strony_mapa($daneBaza[0]);
        }
        if($dane != ''){
            $this->zapisz_do_bazy($dane, $daneBaza[0]['nazwa']);
            $this->pokaz_wyniki($dane, $daneBaza[0]['link']);
        }else{
            echo $tablicaDanych,' - ta strona jest nieaktywna.';
        }
    }
    
    private function pobierz_strony_kategorie($dane){

        if(!isset($this->k)){ $this->k = 0;}
        $html = file_get_html($dane['link'].$dane['kategoria']);
        if($html->find($dane['podkategorieBOX'].$dane['podkategorieZNACZNIK'].$dane['podkategorieNAZWA'])){
            
            foreach ($html->find($dane['podkategorieBOX'].$dane['podkategorieZNACZNIK'].$dane['podkategorieNAZWA'].' '.$dane['tytulBOX'].$dane['tytulZNACZNIK'].$dane['tytulNAZWA'].' '.$dane['tytulELEMENT']) as $row) {
                echo $row->href.'<br>';
                ob_flush(); 
                flush();              
                $dane['kategoria'] = $row->href;
                if(strpos($row->href, $dane['link']) !== false){
                    $dane['link'] = '';
                    $this->pobierz_strony_kategorie($dane);
                }else{
                    $this->pobierz_strony_kategorie($dane);
                }   
            }
        }else{
            $strony = $this->pobierz_strony($dane['link'], $dane['kategoria'], $dane['paginacjaBOX'], $dane['paginacjaZNACZNIK'], $dane['paginacjaNAZWA'], $dane['paginacjaELEMENT']);
            if($strony > 0){
                for($i = 1; $i <= $strony; $i++){
                    echo '<br>strona nr: '.$i.'<br>';
                    $html = file_get_html($dane['link'].$dane['kategoria'].$dane['paginacjaGET'].$i);
                    if($html->find($dane['itemBOX'].$dane['itemZNACZNIK'].$dane['itemNAZWA'])){
                        foreach($html->find($dane['itemBOX'].$dane['itemZNACZNIK'].$dane['itemNAZWA']) as $row){
                            flush();
                            ob_flush();
                            // echo $dane['tytulBOX'].$dane['tytulZNACZNIK'].$dane['tytulNAZWA'].' '.$dane['tytulELEMENT'];
                            $this->daneZwrotne[$this->k]['nazwa'] =  $row->find($dane['tytulBOX'].$dane['tytulZNACZNIK'].$dane['tytulNAZWA'].' '.$dane['tytulELEMENT'], 0)->plaintext;
                            $this->daneZwrotne[$this->k]['cena'] = $row->find($dane['cenaBOX'].$dane['cenaZNACZNIK'].$dane['cenaNAZWA'].' '.$dane['cenaELEMENT'], 0)->plaintext;
                            $this->daneZwrotne[$this->k]['img'] = $row->find($dane['image'], 0)->src;
                            $this->k++;
                        }
                    }
                }
            }else{
                if($html->find($dane['itemBOX'].$dane['itemZNACZNIK'].$dane['itemNAZWA'])){
                    foreach($html->find($dane['itemBOX'].$dane['itemZNACZNIK'].$dane['itemNAZWA']) as $item)
                    {
                        flush();
                        ob_flush();
                        // echo $dane['tytulBOX'].$dane['tytulZNACZNIK'].$dane['tytulNAZWA'].' '.$dane['tytulELEMENT'];
                        $this->daneZwrotne[$this->k]['nazwa'] =  $item->find($dane['tytulBOX'].$dane['tytulZNACZNIK'].$dane['tytulNAZWA'].' '.$dane['tytulELEMENT'], 0)->plaintext;
                        $this->daneZwrotne[$this->k]['cena'] = $item->find($dane['cenaBOX'].$dane['cenaZNACZNIK'].$dane['cenaNAZWA'].' '.$dane['cenaELEMENT'], 0)->plaintext;
                        $this->daneZwrotne[$this->k]['img'] = $item->find($dane['image'], 0)->src;
                        $this->k++;
                    }
                }
            }
            // echo '<pre>';
            // print_r($this->daneZwrotne);
            // echo '</pre>';
            $this->daneZwrotne = [];
            return $this->daneZwrotne;
        }
    }

    private function pobierz_strony_strony($dane){
        $j = 0;
        echo $dane['link'];
        $strony = $this->pobierz_strony($dane['link'], $dane['kategoria'], $dane['paginacjaBOX'], $dane['paginacjaZNACZNIK'], $dane['paginacjaNAZWA'], $dane['paginacjaELEMENT']);
        for($i = 1; $i <= $strony; $i++){
            $html = file_get_html($dane['link'].$dane['kategoria'].$dane['paginacjaGET'].$i);

            if($html->find($dane['itemBOX'].$dane['itemZNACZNIK'].$dane['itemNAZWA'])){
                foreach($html->find($dane['itemBOX'].$dane['itemZNACZNIK'].$dane['itemNAZWA']) as $row){
                    flush();
                    ob_flush();
                    // echo $dane['tytulBOX'].$dane['tytulZNACZNIK'].$dane['tytulNAZWA'].' '.$dane['tytulELEMENT'];
                    $this->daneZwrotne[$j]['nazwa'] =  $row->find($dane['tytulBOX'].$dane['tytulZNACZNIK'].$dane['tytulNAZWA'].' '.$dane['tytulELEMENT'], 0)->plaintext;
                    $this->daneZwrotne[$j]['cena'] = $row->find($dane['cenaBOX'].$dane['cenaZNACZNIK'].$dane['cenaNAZWA'].' '.$dane['cenaELEMENT'], 0)->plaintext;
                    $this->daneZwrotne[$j]['img'] = $row->find($dane['image'], 0)->src;
                    $j++;
                }
            }
        }

        // echo '<pre>';
        // print_r($this->daneZwrotne);
        // echo '<pre>';
        return $this->daneZwrotne;
    }

    private function pobierz_strony_mapa($dane){
        
    }

}