<script>

    function confirmDialogBox(){
        var wyslac = confirm('Czy chcesz wysłać formularz?');
        if(wyslac){
            $.ajax ({
                async: true,
                type: 'POST',
                url: 'zarzadzaj_strona_handler.php',
                data: {dane: tablicaDanych, czynnosc: czynnosc},
                success: function(result){
                    alert(result);
                    alert('Dodano!');
                }

            });
        }
    }

    $(document).ready(function() {

        $('#dodajStrone').click(function() {
            tablicaDanych = {};
            czynnosc = 'dodaj';
            var indeks;
            $('input').each(function(){
                var name = $(this).attr('name');
                tablicaDanych[name] = $(this).val()
                indeks++;
            });
            console.log(tablicaDanych);
            confirmDialogBox();
        });

    });

</script>


<?php
include 'polaczenie_baza.php';
include 'class_ZarzadzajStrona.php';

echo '<h2>Dodaj stronę</h2>';


echo '<form id="dodajStroneForm" method="post">';
        echo '<p>Jaką stronę chcesz dziś dodać? :)<br>';
        echo '<table>';
            echo '<tr>';
                echo '<td>';
                    echo 'Nazwa: ';
                echo '</td>';
                echo '<td>';
                    echo '<input name="nazwa" type="text" value="">';
                echo '</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<td>';
                    echo 'Link: ';
                echo '</td>';
                echo '<td>';
                    echo '<input name="link" type="text" value="">';
                echo '</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<td>';
                    echo 'Kategoria: ';
                echo '</td>';
                echo '<td>';
                    echo '<input name="kategoria" type="text" value="">';
                echo '</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<td>';
                    echo 'Podkategorie: ';
                echo '</td>';
                echo '<td>';
                    echo '<input name="podkategorieBOX" type="text" value="" placeholder="div">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="podkategorieZNACZNIK" type="text" value="" placeholder="class">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="podkategorieNAZWA" type="text" value="" placeholder="nazwa">';
                echo '</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<td>';
                    echo 'Item: ';
                echo '</td>';
                echo '<td>';
                    echo '<input name="itemBOX" type="text" value="" placeholder="div">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="itemZNACZNIK" type="text" value="" placeholder="class">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="itemNAZWA" type="text" value="" placeholder="nazwa">';
                echo '</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<td>';
                    echo 'Paginacja: ';
                echo '</td>';
                echo '<td>';
                    echo '<input name="paginacjaBOX" type="text" value="" placeholder="div">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="paginacjaZNACZNIK" type="text" value="" placeholder="class">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="paginacjaNAZWA" type="text" value="" placeholder="nazwa">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="paginacjaELEMENT" type="text" value="" placeholder="szukany znacznik">';
                echo '</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<td>';
                    echo 'Tytul: ';
                echo '</td>';
                echo '<td>';
                    echo '<input name="tytulBOX" type="text" value="" placeholder="div">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="tytulZNACZNIK" type="text" value="" placeholder="class">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="tytulNAZWA" type="text" value="" placeholder="nazwa">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="tytulELEMENT" type="text" value="" placeholder="szukany znacznik">';
                echo '</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<td>';
                    echo 'Cena: ';
                echo '</td>';
                echo '<td>';
                    echo '<input name="cenaBOX" type="text" value="" placeholder="div">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="cenaZNACZNIK" type="text" value="" placeholder="class">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="cenaNAZWA" type="text" value="" placeholder="nazwa">';
                echo '</td>';
                echo '<td>';
                    echo '<input name="cenaELEMENT" type="text" value="" placeholder="szukany znacznik">';
                echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
                echo 'Obraz: ';
            echo '</td>';
            echo '<td>';
                echo '<input name="image" type="text" value="" placeholder="szukany znacznik">';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
                echo '<td>';
                    echo '<button id="dodajStrone" type="button" name="dodajStrone" value="">Wyślij</button>';
                echo '</td>';
            echo '</tr>';

        echo '</table>';
    echo '</form>';

// if(isset($_POST['dodajStrone'])){
//     $nowaStrona = new ZarzadzajStrona();
//     $wynik = $nowaStrona->dodaj_strone();
//     echo $wynik;
// }
// $nowaStrona = new ZarzadzajStrona();

// $nowaStrona->dodaj_strone('strona');

