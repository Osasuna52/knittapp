KnittApp is meant to be simple application that allows to add and browse pages in search of yarns.

It's made for learning purposes.

**To do:**
- ~~add ability to save downloaded data in database~~
- add filtering options
    - by name
    - by price
    - by color
- ~~add the possibility of editing~~
- add two new ways to add pages
- add support for map-based websites
- ~~add logs~~
- add the ability to display or download one (or more) pages
- add automatically download
- add email notifications
- add notifications of price reductions
- add "robots.txt" handling
- add random time and page delay to prevent from blocking and throttling
- maybe add some "random clicks/visits" to act like we are a human and we aren't using the same browse pattern
- rotate IP and proxy services if possible (sites can collect data)
- try to fake user-agent to prevent beeing taken as bot
- chceck if some pages arent slighlty changed to prevent scrapping (x-paths)
- headless browser?
- learn to avoid honeypods - like display:none links or different visibility or nofollow tag
- filtering through database - not javascript
- decide: download images from page (???) or just links and allow user to enable/disable them (traffic problem with getting eg 2000 images from page)