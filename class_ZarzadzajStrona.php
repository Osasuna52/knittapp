<?php

include 'class_KnittApp.php';

class ZarzadzajStrona {

    public $link;
    public $idStrony;

	function __construct($strona = null, $idStrony = 0){
		if ( $strona == null) {
			$this->strona = $strona;
        }
        $this->idStrony = $idStrony;
        $this->polaczenie = $this->polacz_baza();
        $this->knittApp = new knittApp();
    }

    private function polacz_baza(){
        include 'polaczenie_baza.php';
        $this->connect = $connect;
    }

    public function zaladuj_wloczki(){
        $strony = $this->knittApp->dane_baza();
        $wloczki = [];
        foreach($strony as $id => $value){
            $zptWloczki = "SELECT * FROM wloczki_szczeg WHERE nazwa_strony = '".$strony[$id]['nazwa']."'";
            $result = mysqli_query($this->connect, $zptWloczki);
            if(mysqli_num_rows($result)>0){
                $i = 0;
                while($wynik = mysqli_fetch_assoc($result)){
                    $wloczki[$i]['nazwa_strony'] = $wynik['nazwa_strony'];
                    $wloczki[$i]['nazwa_wloczki'] = $wynik['nazwa_wloczki'];
                    $wloczki[$i]['cena'] = $wynik['cena'];
                    $wloczki[$i]['obraz'] = $wynik['obraz'];
                    $wloczki[$i]['data_aktualizacji'] = $wynik['data_dodania'];
                    $i++;
                }
            }
        }
        return $wloczki;
    }

    public function pokaz_wyniki($dane){
        $i = 1;
        $str = '<div class="wloczkiWszystkie">';
        foreach($dane as $id => $indeksy){
            $str .= '<div class="wloczkaBox">';
                $str .= $i;
                $str .= '<input name="nazwa_strony" type="hidden" value="'.$indeksy['nazwa_strony'].'">';
                // $str .= '<img src="'.$indeksy['obraz'].'"/>';
                $str .= '<p>'.$indeksy['nazwa_wloczki'].'</p>';
                $str .= '<span>Cena: <input name="cena" type="text" readonly value="'.$indeksy['cena'].'"></span>';
                $str .= '<input name="data_aktualizacji" type="hidden" value="'.$indeksy['data_aktualizacji'].'">';
            $str .= '</div>';
            $i++;
        }
        $str .= '</div>';
        return $str;
    }

    public function dane_baza(){
        return $this->knittApp->dane_baza();
    }
    
    public function dodaj_strone($daneDodaj){
        
        $pola = '';
        $wartosci = '';
        foreach($daneDodaj as $nazwa => $wartosc) {
            $pola .= '`'.$nazwa.'`, ';
            $wartosci .= '"'.$wartosc.'", ';
        }
        $pola = substr($pola, 0, strlen($pola)-2);
        $wartosci = substr($wartosci, 0, strlen($wartosci)-2);
        $zpt = "INSERT INTO strony_baza (".$pola.") VALUES (".$wartosci.")";
        $result = mysqli_query($this->connect, $zpt);
        return $result;
    }


}