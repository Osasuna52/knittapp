<?php 
session_start();
if(!isset($_SESSION['motyw'])){
    $_SESSION['motyw'];
}
?>
<script src="js/jquery-3.4.1.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script> -->
<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="css/style.css">
<script>
    
    $(document).ready(function(){
        $('#ladujWloczki').click(function() {
            $('#ladujWloczkiZgoda').val('tak');
            $('#ladujWloczkiForm').submit();
        });
        $('#dodajStrone').click(function() {
            $('#dodajStroneZgoda').val('tak');
            $('#ladujWloczkiForm').submit();
        });
        $('#pokazWloczki').click(function() {
            $('#pokazWloczkiZgoda').val('tak');
            $('#ladujWloczkiForm').submit();
        });
        $('#pokazSamouczek').click(function() {
            $('#pokazSamouczekZgoda').val('tak');
            $('#ladujWloczkiForm').submit();
        });
        
       
    });
</script>

<?php

include 'auto_load.php';

        // $_SESSION['motyw'] = 'jasny';

include 'polaczenie_baza.php';
if(isset($_POST['laduj_wloczki']) && $_POST['laduj_wloczki'] == 'tak'){
    include 'laduj_wloczki.php';
}elseif(isset($_POST['dodaj']) && $_POST['dodaj'] == 'tak'){
    include 'zarzadzaj_strona.php';
}elseif(isset($_POST['pokaz_wloczki']) && $_POST['pokaz_wloczki'] == 'tak'){
    include 'pokaz_wloczki_z_bazy.php';
}elseif(isset($_POST['samouczek']) && $_POST['samouczek'] == 'tak'){
    include 'samouczek.php';
}else{

    echo '<form id="ladujWloczkiForm" method="post">';
        // echo '<p>Chcesz załadować włóczki?</p>';
        echo '<input id="pokazWloczki" type="button" value="Wyświetl dane">';
        echo '<input id="ladujWloczki" type="button" value="Ładuj dane">';
        echo '<input id="dodajStrone" type="button" value="Dodaj stronę">';
        echo '<input id="pokazSamouczek" type="button" value="Samouczek">';
        echo '<input type="hidden" id="dodajStroneZgoda" name="dodaj" value="nie">';
        echo '<input type="hidden" id="ladujWloczkiZgoda" name="laduj_wloczki" value="nie">';
        echo '<input type="hidden" id="pokazWloczkiZgoda" name="pokaz_wloczki" value="nie">';
        echo '<input type="hidden" id="pokazSamouczekZgoda" name="samouczek" value="nie">';
    echo '</form>';

    $zpt = "SELECT nazwa, data_aktualizacji FROM strony_log INNER JOIN strony_baza ON strony_baza.id_strona=strony_log.id_strony WHERE data_aktualizacji IN (SELECT MAX(data_aktualizacji) FROM strony_log GROUP BY id_strony)ORDER BY data_aktualizacji DESC";
    $result = mysqli_query($connect, $zpt);
    
    echo '<div id="ostatniaAktualizacja">';
        echo '<p>Ostatnia aktualizacja</p>';
        while($wynik = mysqli_fetch_assoc($result)){
            echo '<div class="aktualizacjaContainer">';
            echo '<div class="nazwaStrony">'.$wynik['nazwa'].'</div>';
            echo '<div class="dataAktualizacji">'.$wynik['data_aktualizacji'].'</div>';
            echo '</div>';
        }
    echo '</div>';
    include 'wyswietl_zapisane_strony.php';

}
?>