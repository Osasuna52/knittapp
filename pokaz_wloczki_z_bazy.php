<script>
$(function() {
    $('.siteFilter').click(function() {

    });

    $('#alfabetycznieF').click(function() {
        $nazwy = $('.wloczkaBox');
        var alfabetycznie = $nazwy.sort(function(a, b) {
            return $(a).find("p").text() > $(b).find("p").text();
        });
        $(".wloczkiWszystkie").html(alfabetycznie);
    });
    $('#alfabetycznieR').click(function() {
        $nazwy = $('.wloczkaBox');
        var alfabetycznie = $nazwy.sort(function(a, b) {
            return $(a).find("p").text() < $(b).find("p").text();
        });
        $(".wloczkiWszystkie").html(alfabetycznie);
    });
    $('#cenaR').click(function() {
        $nazwy = $('.wloczkaBox');
        var cenowo = $nazwy.sort(function(a, b) {
            var liczba1 = $(a).find("[name='cena']").val();
            var liczba2 = $(b).find("[name='cena']").val();
            return parseFloat(liczba1) > parseFloat(liczba2);
        });
        $(".wloczkiWszystkie").html(cenowo);
    });
    $('#cenaM').click(function() {
        $nazwy = $('.wloczkaBox');
        var cenowo = $nazwy.sort(function(a, b) {
            var liczba1 = $(a).find("[name='cena']").val();
            var liczba2 = $(b).find("[name='cena']").val();
            return parseFloat(liczba1) < parseFloat(liczba2);
        });
        $(".wloczkiWszystkie").html(cenowo);
    });

    $('#otworzFiltry').click(function() {
        if ($(this).css('marginRight') == '250px') {
            $('#filterC').css('width', '0');
            $('#otworzFiltry').css('margin-right', '0');
        } else {
            $('#filterC').css('width', '250px');
            $('#otworzFiltry').css('margin-right', '250px');
        }
    });
    $('.zamknijFiltry').click(function() {
        $('#filterC').css('width', '0');
        $('#otworzFiltry').css('margin-right', '0');
    });
});
</script>

<?php
include 'class_ZarzadzajStrona.php';
echo '<h2>Zapisane włóczki</h2>';
$zarzadzaj = new ZarzadzajStrona();
$wloczki = $zarzadzaj->zaladuj_wloczki();

$daneDoWyswietlenja = $zarzadzaj->pokaz_wyniki($wloczki);
echo $daneDoWyswietlenja;
?>
<div style="clear: both;"></div>

<div id="otworzFiltry">
    <button class="filtry">☰ Filtry</button>
</div>
<div class="filterContainer" id="filterC">
    <div class="zamknijFiltry">×</div>

    <!-- <div class="siteFilter">strona</div> -->
    <!-- <div class="priceFilter">cena</div>
    <div class="colorFilter">kolor</div> -->
    <div class="nameFilter filter">
        <a class="descBox">NAZWY</a>
        <div class="contBox">
            <a><div id="alfabetycznieF">A-Z,&nbsp;0-9</div></a>
            <a><div id="alfabetycznieR">Z-A,&nbsp;9-0</div></a>
        </div>
    </div>
    <div class="priceFilter filter">
        <a class="descBox">CENY</a>
        <div class="contBox">
            <a><div id="cenaR">Ceny&nbsp;rosnąco</div></a>
            <a><div id="cenaM">Ceny&nbsp;malejąco</div></a>
        </div>
    </div>
</div>
