<?php
if(!isset($_SESSION['motyw'])){
    $_SESSION['motyw'] = 'jasny';
}
include 'simplehtmldom/simple_html_dom.php';
include 'class_KnittApp.php';
include 'polaczenie_baza.php';
?>
<script>
    $(document).ready(function() {
        var hours = 0;
        var minutes = 0;
        var seconds = 0;
        var counter = 0;

        function pad(num) {
            if (num < 10 && num >= 0) return "0" + num;
            else return num;
        }

        function tictac() {
            counter++;
            seconds = counter;
            if(seconds >= 59){
                minutes++;
                seconds = 0;
                counter = 0;
            }
            if(minutes >= 59){
                hours++;
                minutes = 0;
            }
            $("#timer").html(pad(hours) + ":" + pad(minutes) + ":" + pad(seconds));
        }

        $("#checkAll").click(function() {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        var timer = undefined;

        $('#start').on('click', function() {
            timer = window.setInterval(tictac, 1000);
        });
        $('#stop').on('click', function() {
            timer = window.clearInterval(timer);
        });
        $('#reset').on('click', function() {
            window.clearInterval(timer);
            counter = 0;
            seconds = 0;
            minutes = 0;
            hours = 0;
            $("#timer").html(pad(hours) + ":" + pad(minutes) + ":" + pad(seconds));
        });


        $("#ladujWybrane").click(function() {
            window.clearInterval(timer);
            counter = 0;seconds = 0;minutes = 0;hours = 0;
            timer = window.setInterval(tictac, 1000);
            var czynnosc = 'ladujPojedynczo';
            $("input[type='checkbox']").not(':eq(0)').each(function(){
                if($(this).is(':checked')){
                    ilosc++;
                    $('.content').append('Ładuję '+ $(this).val() + '<br>');
                    $.ajax ({
                        async: true,
                        type: 'POST',
                        cache: false,
                        url: 'knittapp_handler.php',
                        data: {dane: $(this).val(), czynnosc: czynnosc},
                        success: function(result){
                            $('.content').append('<br>'+result);
                            timer = window.clearInterval(timer);
                        }
                    }).then(function(){
                    });   
                }else{}
            });
        });
        $("#ladujWszystko").click(function() {
            window.clearInterval(timer);
            counter = 0;seconds = 0;minutes = 0;hours = 0;
            timer = window.setInterval(tictac, 1000);
            var czynnosc = 'ladujWszystko';
            $('.content').append('<br> Ładuję wszystkie strony naraz. To może zająć dłuższą chwilę...');
            $.ajax ({
                async: true,
                type: 'POST',
                cache: false,
                url: 'knittapp_handler.php',
                data: {dane: '', czynnosc: czynnosc},
                success: function(result){
                    $('.content').append('<br>'+result);
                    window.clearInterval(timer);
                }
            }).then(function(){
            });            
            
        });
        

    });
</script>

<div id="timer">00:00:00</div>
<div class="buttons timerButtons" class="clear">
    <input type="button" class="button" id="start" value="start" />
    <input type="button" class="button" id="stop" value="stop" />
    <input type="button" class="button" id="reset" value="reset" />
</div>

<div class="buttons">
    <input id="ladujWszystko" type="button" value="Ładuj wszystko">
    <?php
    $zpt = "SELECT * FROM strony_baza WHERE active = 1 ORDER BY nazwa";
    $result = mysqli_query($connect, $zpt);
    if (mysqli_num_rows($result) > 0) {
        echo '<table class="checkboxContainer">';
        echo '<tr><td><input id="checkAll" type="checkbox" name="stronaDoZaladowania"></td><td> Zaznacz wszystkie </td></tr>';
        while ($wynik = mysqli_fetch_assoc($result)) {
            echo '<tr><td><input type="checkbox" name="stronaDoZaladowania" value="' . $wynik['nazwa'] . '"></td><td>' . $wynik['nazwa'] . '</td></tr>';
        }
        echo '</table>';
    }
    ?>
    <input id="ladujWybrane" type="button" value="Ładuj wybrane">
</div>
<div style="clear: both;"></div>

<p style="margin-bottom: 0; margin-left: 20px;">Załadowane dane:</p>
<div class="content">

</div>


<?php
echo '<br>';
$executionTime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
echo '<b>Czas ładowania strony:</b> ' . $executionTime . ' s';

?>